import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent} from './pages/dashboard/dashboard.component';
import { ServiceSearchComponent } from './pages/serviceSearch/serviceSearch.component';

const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'serviceSearch', component: ServiceSearchComponent },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
