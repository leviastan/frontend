import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Headers, RequestOptions, Http } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/toPromise';
import * as _ from 'lodash';

/* service for enabled data */
@Injectable()
export class DataService {
  constructor (private http: Http) {
  }

  /**
   * get the header
   * @return {Headers}
   */
  getHeaders(): Headers {
    const headers = new Headers();
    headers.append('username', environment.username);
    return headers;
  }

  /**
   * get serviceProviders
   * @return {Promise<any>}
   */
  getServiceProviders (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/serviceProviders`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * get services
   * @return {Promise<any>}
   */
  getServiceVersions (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/serviceVersions`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * get requesting system
   * @return {Promise<any>}
   */
  getRequestingSystems (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/requestingSystems`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * get transmission type
   * @return {Promise<any>}
   */
  getTransmissionType (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/transmissionTypes`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * get statuses type
   * @return {Promise<any>}
   */
  getStatuses (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/statuses`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * get message Types
   * @return {Promise<any>}
   */
  getMessageTypes (): Promise<any> {
    return this.http.get(`${environment.baseUri}/lookup/messageTypes`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * search service
   * @param params
   * @return {Promise<any>}
   */
  searchService (params): Promise<any> {
    return this.http.get(`${environment.baseUri}/services`, new RequestOptions({ params }))
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * search message
   * @param params
   * @return {Promise<any>}
   */
  searchMessage (params): Promise<any> {
    let httpParams = new HttpParams();
    _.forEach(_.keys(params), key => {
      if (_.isArray(params[key])) {
        for (let i = 0; i < params[key].length; i += 1) {
          if (_.isObject(params[key][i])) {
             _.forEach(_.keys(params[key][i]), name => {
               httpParams = httpParams.append(`${key}[${i}].${name}`, params[key][i][name])
             });
          }
        }
      } else {
        httpParams = httpParams.append(key, params[key]);
      }
    });
    return this.http.get(`${environment.baseUri}/messages?${httpParams.toString()}`)
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }

  /**
   * replay message by message ids
   * @param body
   * @return {Promise<Response>}
   */
  replayMessage (body): Promise<any> {
    return this.http.post(`${environment.baseUri}/messages/replay`, body, new RequestOptions({ headers: this.getHeaders() }))
      .toPromise();
  }

  /**
   * get error message
   * @param {[number]} err error status
   */
  getErrorMessage (err) {
    try {
      const errJson = err.json();
      if (errJson && errJson.message) {
        return errJson.message;
      }
      return JSON.stringify(err);
    } catch (e) {
      return err.message;
    }
  }

  /**
   * common handle error
   * @param  {any}          error [description]
   * @return {Promise<any>}       [description]
   */
  private handleError (error: any): Promise<any> {
    return Promise.reject(error.statusText || error);
  }
}
