import { Component } from '@angular/core';
import { Router } from '@angular/router';

/**
 * This class represents the lazy loaded HeaderComponent.
 */
@Component({
  selector: 'app-sd-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss']
})
export class HeaderComponent {
  /**
   * Creates an instance of the HeaderComponent with the injected router
   *
   */
  constructor (private router: Router) {
  }

  onDashboard() {
    this.router.navigate(['dashboard']);
  }

  onServiceSearch() {
    this.router.navigate(['serviceSearch']);
  }
}
